package com.zkdb.selenium.tset;

import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.zkdb.selenium.util.ElementLocateMode;
import com.zkdb.selenium.util.ExcelReader;
import com.zkdb.selenium.util.InitDriver;
import com.zkdb.selenium.util.Login;
import com.zkdb.selenium.util.SeleniumUtil;
import com.zkdb.selenium.util.WaitiElementsLoad;
import com.zkdb.selenium.vo.UserAccountVO;

public class ReimbursementTest {

    
    public static void main(String[] args) {
//        String excelFileName ="C:\\Users\\A1588\\Desktop\\UserAccountVO.xlsx";
//        List<UserAccountVO> userDate =ExcelReader.readExcel(excelFileName);
//        for (UserAccountVO userAccountVO : userDate) {
//            System.out.println(userAccountVO);
//        }
        
        SimpleDateFormat format =new SimpleDateFormat("yyyy-MM-dd(HH:mm:ss)");
        String path = format.format(System.currentTimeMillis());
        String curPath = System.getProperty("user.dir");
        path =path+".png";
        String scrrrnPath = curPath+"/"+path;
        System.out.println(scrrrnPath);
    }
    
    public static void run() {
        // TODO Auto-generated method stub
        WaitiElementsLoad load = new WaitiElementsLoad();
        SeleniumUtil util = new SeleniumUtil();
        Logger logger =Logger.getLogger(ReimbursementTest.class);
        
        WebDriver driver =InitDriver.INSTANCE.getDriver();
        
        Login login = new Login();
        login.loginDevelopmentAccount(driver, "15882891378", "888888");
     
        try {
            Thread.sleep(2000);
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        login.loginTestAccount(driver, "BEP", "1008483", "11111111");
        logger.info("登陆账号:1008483");
        try {

            
           String processName ="费用报销-支费[2020]0050-金额：1,099.00-王熙凤";

            
            
            
            
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("body")));
            try {
                Thread.sleep(6500);
            }
            catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            load.Wait(driver, 10, ElementLocateMode.FIND_ELEMENT_ID, "webiframe");
            driver.switchTo().frame("webiframe");
            
            load.Wait(driver,30,ElementLocateMode.FIND_ELEMENT_CSSSELECTOR,".fa-tasks");
            //new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(By.id("a_number3")));
            driver.findElement(By.cssSelector(".fa-tasks")).click();
            logger.info("-----------点击工作箱------------");
            
            load.Wait(driver, 10, ElementLocateMode.FIND_ELEMENT_NAME, "workframe");
            driver.switchTo().frame("workframe");
            
            load.Wait(driver,30,ElementLocateMode.FIND_ELEMENT_CSSSELECTOR,".working .glyphicon");
            driver.findElement(By.cssSelector(".working .glyphicon")).click();
            logger.info("-----------点击工作箱搜索------------");
            
            load.Wait(driver,10,ElementLocateMode.FIND_ELEMENT_LINKTEXT,"自定义搜索");
            driver.findElement(By.linkText("自定义搜索")).click();
            
            load.Wait(driver,30,ElementLocateMode.FIND_ELEMENT_XPATH,"//div[4]/div/div[2]/div/input");
            driver.findElement(By.xpath("//div[4]/div/div[2]/div/input")).click();
            driver.findElement(By.xpath("//div[4]/div/div[2]/div/input")).sendKeys(processName);
            
            logger.info("-----------点击事项标题------------");
            
            
            load.Wait(driver,30,ElementLocateMode.FIND_ELEMENT_CSSSELECTOR,".querybtn");
            driver.findElement(By.cssSelector(".querybtn")).click();
            logger.info("-----------点击查询------------");
            Thread.sleep(2000);
            String text="";
            By elementGroup = new By.ByCssSelector(".wftasks .name");
            
            By elementUnclassified = new By.ByCssSelector("#box_data .name");
            
            boolean flag =true; 
            if(util.checkExistsElement(driver, elementGroup)) {
                load.Wait(driver,30,ElementLocateMode.FIND_ELEMENT_CSSSELECTOR,".wftasks .name");
                text=  driver.findElement(By.cssSelector(".wftasks .name")).getText();
                logger.info("-----------分组------------"+text);
                
            }else if (util.checkExistsElement(driver, elementUnclassified)) {
                load.Wait(driver,30,ElementLocateMode.FIND_ELEMENT_CSSSELECTOR,"#box_data .name");
                text=  driver.findElement(By.cssSelector("#box_data .name")).getText();
                flag=false;
                logger.info("-----------未分组------------"+text);
            }
            
            
            
            
            
            
            //判断是否存在
            if (processName.equals(text)) {
                if (flag) {
                    driver.findElement(By.cssSelector(".wftasks .name")).click();
                }
                else {
                    driver.findElement(By.cssSelector("#box_data .name")).click();
                }
                
                
                
                String handle= driver.getWindowHandle();

                util.switchWindow(driver);
                Thread.sleep(4000);
                By unreadMessages = new By.ByCssSelector(".close");
                if(util.checkExistsElement(driver, unreadMessages)) {
                    logger.info("-----------@未阅消息提醒------------");
                    load.Wait(driver,30,ElementLocateMode.FIND_ELEMENT_CSSSELECTOR,".close");
                    driver.findElement(By.cssSelector(".close")).click();
                }
                //点击批转
                load.Wait(driver,60,ElementLocateMode.FIND_ELEMENT_ID,"form_transmitNext");
                driver.findElement(By.id("form_transmitNext")).click();
                load.Wait(driver,60,ElementLocateMode.FIND_ELEMENT_ID,"Transmit_userTree");
                WebElement element =driver.findElement(By.id("Transmit_userTree"));
                element.isDisplayed();
                List<WebElement> elements= element.findElements(By.tagName("span"));
                
                for (WebElement webElement : elements) {
                    if(webElement.getAttribute("data-nodetype")!=null) {
                       
                       if (webElement.getAttribute("data-user_userid")!=null) {
                        System.out.println(webElement.getAttribute("id"));
                        System.out.println(webElement.getAttribute("data-user_userid"));
                        System.out.println(webElement.getAttribute("data-user_username"));
                        System.out.println(webElement.getAttribute("id").replace("span", "check"));
                        driver.findElement(By.id(webElement.getAttribute("id").replace("span", "check"))).click();
                       }
                    }
                }
                
            }
            
            
           


            


            
        }catch (Exception e){
            System.out.println(e.toString());
            SeleniumUtil.runExceptionScreenshot(driver);
        }
        finally {
            // TODO: handle finally clause
            try {
                Thread.sleep(6200);
            }
            catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            driver.quit();
        }
    }

}
