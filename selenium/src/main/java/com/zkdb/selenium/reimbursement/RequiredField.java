package com.zkdb.selenium.reimbursement;

/**
 * 
 * @ClassName: RequiredField 
 * @Description: TODO(表单必填字段) 
 * @author tangxiaoyu 
 * @date 2020年3月24日 下午2:03:28 
 *
 */
public class RequiredField {
    /**
     * 数据集id
     */
    private String dataId;
    /**
     * 数据集字段
     */
    private String field;
    /**
     * 数据集字段名称
     */
    private String fieldName;
    
    public String getDataId() {
        return dataId;
    }
    public void setDataId(String dataId) {
        this.dataId = dataId;
    }
    public String getField() {
        return field;
    }
    public void setField(String field) {
        this.field = field;
    }
    public String getFieldName() {
        return fieldName;
    }
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
    @Override
    public String toString() {
        return "RequiredField [dataId=" + dataId + ", field=" + field + ", fieldName=" + fieldName + "]";
    }
    public RequiredField(String dataId, String field, String fieldName) {
        super();
        this.dataId = dataId;
        this.field = field;
        this.fieldName = fieldName;
    }
    
    
    
}
